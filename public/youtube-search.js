// http://senticoding.tistory.com/37
function search(pageToken) {
    var q = $("#query").val();
    if (q == "") {
        alert("검색어를 입력하세요.");
        $("#query").focus();
        return;
    }
    $("#search-container").empty();
    $("#nav-container").empty();

    // Search list
    // https://developers.google.com/youtube/v3/docs/search/list
    var youtubeApiUrl = "https://www.googleapis.com/youtube/v3/search?";
    var paramOptions = {
        key: "AIzaSyCsfp9gCaUvCG8sbfremtp7Iqa1hBT1IvY",
        part: "snippet",
        q: encodeURIComponent(q),
        maxResults: 10,  // 페이지당 검색 결과 수
        regionCode: "KR",
        order: "relevance"
        // order: "viewCount"
    };

    for (var option in paramOptions) {
        youtubeApiUrl += option + "=" + paramOptions[option] + "&";
    }

    if (pageToken) {
        youtubeApiUrl += "pageToken=" + pageToken;
    }

    $.ajax({
        type: "POST",
        url: youtubeApiUrl,
        dataType: "jsonp",
        success: function (jdata) {
            console.log(jdata);

            $(jdata.items).each(function (i) {
                // Search list, JSON results
                // https://developers.google.com/youtube/v3/docs/search#resource
                $("#search-container").append("<p class='box'>" +
                    "<a href='https://youtu.be/" + this.id.videoId + "' target='_blank'>" +
                    "<img src='" + this.snippet.thumbnails.default.url + "'>" +
                    "<br> <span>" + this.snippet.title + "</span></a>" +
                    "<br> <span>" + this.snippet.channelTitle + "</span>" +
                    " • <span>" + (new Date(this.snippet.publishedAt)).toLocaleString() + "</span>" +
                    "<br> <span>" + this.snippet.description + "</span>" +
                    "</p>");
            }).promise().done(function () {
                if (jdata.prevPageToken) {
                    $("#nav-container").append("<a href='javascript:search(\"" + jdata.prevPageToken + "\");'><이전페이지></a>");
                }
                if (jdata.nextPageToken) {
                    $("#nav-container").append("<a href='javascript:search(\"" + jdata.nextPageToken + "\");'><다음페이지></a>");
                }
            });
        },
        error: function (xhr, textStatus) {
            console.log(xhr.responseText);
            alert("지금은 시스템 사정으로 인하여 요청하신 작업이 이루어지지 않았습니다.\n잠시후 다시 이용하세요.[2]");
            return;
        }
    });
}